import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { pagesReducer, themesReducer, filesReducer, messageReducer } from '../reducer';
import rootSaga from '../sagas';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(combineReducers({
    pages: pagesReducer, themes: themesReducer,
    files: filesReducer, messages: messageReducer
}),
    applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

export default store;

import { takeEvery, all, put, call } from "redux-saga/effects";
import { putData, deleteData } from '../storage';
/*
Saga responsável por monitorar a quantidade atual de arquivos
*/
function* uploadFile(action) {
    try {
        const res = yield call(putData, JSON.stringify(action.data));
        console.log(res);
        yield put({ type: 'ADD_FILE', data: res.length });//Seta quantidade ao fazer novo upload
        yield put({ type: 'SUCCESS', value: 'Upload realizado com sucesso' });//Retorna mensagem de sucesso
    } catch (err) {
        yield put({ type: 'ERROR', value: 'Erro ao fazer upload' });//Retorna mensagem de erro
    }

}

function* deleteFile(action) {
    try {
        const res = yield call(deleteData, JSON.stringify(action));
        console.log(res);
        yield put({ type: 'ADD_FILE', data: res.length });//Seta quantidade ao excluir um upload
        yield put({ type: 'SUCCESS', value: 'Arquivo excluído com sucesso' });//Retorna mensagem de sucesso
    } catch (err) {
        yield put({ type: 'ERROR', value: 'Erro ao excluir arquivo'  });//Retorna mensagem de erro
    }
}

export default function* root() {
    yield all([
        takeEvery('ASYNC_ADD_FILE', uploadFile),
        takeEvery('ASYNC_DELETE_FILE', deleteFile),
    ]);
}
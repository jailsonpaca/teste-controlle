import INITIAL_STATE from "./initialState";
//Reducer responsável pelas notificações de erro ou sucesso
export default function messages(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'ERROR':
            return { ...state, messages: { value: action.value, error: true } }
        case 'SUCCESS':
            return { ...state, messages: { value: action.value, error: false } }
        default:
            return state;
    }
}

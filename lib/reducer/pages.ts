import INITIAL_STATE from "./initialState";
//Reducer responsável pelos botões de navegação do menu
export default function pages(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'CHANGE_PAGE':
            return { ...state, page: action.value }
        default:
            return state;
    }
}

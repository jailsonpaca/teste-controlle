const INITIAL_STATE = {
    theme: 'light',
    page: '1',
    files: 0, // Quantidade atual de arquivos igual a zero
    messages: { value: '', error: false },
}

export default INITIAL_STATE;
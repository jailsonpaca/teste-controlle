import INITIAL_STATE from "./initialState";
//Reducer responsável pela quantidade atual de arquivos
export default function files(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'ADD_FILE':
            return { ...state, files: action.data }
        default:
            return state;
    }
}

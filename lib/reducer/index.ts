import pagesReducer from './pages';
import themesReducer from './themes';
import filesReducer from './files';
import messageReducer from './messages';
export { pagesReducer, themesReducer, filesReducer, messageReducer };
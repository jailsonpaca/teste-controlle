import INITIAL_STATE from "./initialState";
//Reducer responsável pela mudança de temas entre "dark" e "light"
export default function themes(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'CHANGE_THEME':
            return { ...state, theme: action.value }
        default:
            return state;
    }
}
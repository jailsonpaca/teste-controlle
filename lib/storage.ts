import axios from 'axios';
const url = 'http://localhost:3001/api/v1/attachments';

async function getData() { //Solicita os arquivos enviados
    return new Promise(async (resolve, reject) => {
        await axios.get(url).then(async res => {
            resolve(res.data.values);
        }).catch(err => {
            console.log(err);
            reject(err);
        });
    });
}

async function putData(data) { //Envia um novo arquivo
    return new Promise(async (resolve, reject) => {
        await axios({ url, method: 'POST', data, headers: { 'Content-Type': 'application/json', } }).then(async res => {
            resolve(res.data);//Retorna mensagem de sucesso com a atual quantidade de arquivos
        }).catch(err => {
            console.log(err);
            reject(err);
        });
    });
}

async function deleteData(data) {//Exclui um arquivo
    return new Promise(async (resolve, reject) => {
        await axios({ url: url + '/delete', method: 'POST', data, headers: { 'Content-Type': 'application/json', } })
            .then(async res => {
                resolve(res.data);//Retorna mensagem de sucesso com a atual quantidade de arquivos
            }).catch(err => {
                console.log(err);
                reject(err);
            });
    });
}

export {
    getData, putData, deleteData
}
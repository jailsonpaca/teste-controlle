import type { AppProps } from 'next/app';
import { Provider } from 'react-redux';
import store from '../lib/store';
import Head from 'next/head';
import styles from '../styles/Home.module.css';
import '../styles/globals.css'
import "../styles/antd.less";
import LayoutComponent from '../components/layout';

function App({ Component, pageProps }: AppProps) {
  /* Provider do Redux para estado Global */
  return (<Provider store={store} >
    <Head>
      <title>Teste Prático - Controlle</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <div className={styles.container}>
      <LayoutComponent>
        <Component {...pageProps} />
      </LayoutComponent>
    </div>
  </Provider>)
}

export default App;

import React, { useEffect, useState } from 'react';
import { Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { useDispatch } from 'react-redux';
import Dropzone from 'react-dropzone';
import CardItem from '../components/cardItem';

function getBase64(file): Promise<string | ArrayBuffer> {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

const MakeUpload: React.FC = () => {
    /* Página para fazer uploads */
    const [fileList, setFileList] = useState<any>([]);
    const [fileToUpload, setFileToUpload] = useState<any>(null);
    const uploadFile = useDispatch();

    async function handleChange(files) {

        setFileList(prev => { return ([...prev, ...files]) });
        const base64 = await getBase64(files[0]);
        setFileToUpload({ base64, name: files[0].name, type: files[0].type })
    }

    useEffect(() => {
        const runUpload = () => {
            uploadFile({
                type: 'ASYNC_ADD_FILE',
                data: { ...fileToUpload }
            });
        };
        if (fileToUpload !== null) {
            runUpload();
        }

    }, [fileToUpload]);

    return (<>
        <div style={{
            display: 'flex', flexWrap: 'wrap', justifyContent: 'center',
            alignContent: 'center', height: '100%'//Optei por CSS puro em algumas partes para prova de conhecimento
        }}>
            {fileList.map((file, i) => (
                <CardItem key={i} styles={{ cursor: 'auto', width: 240, height: 240, margin: '1%' }}
                    source={getBase64(file)} name={file.name} description={file.type} isPreview />
            ))}
            <Dropzone onDrop={acceptedFiles => handleChange(acceptedFiles)}>
                {({ getRootProps, getInputProps }) => (
                    <div {...getRootProps()}>
                        <CardItem styles={{ width: 240, minHeight: 200, margin: '1%' }} isImage={false} >
                            <Button type="text" block style={{ height: 150 }} ><PlusOutlined style={{ fontSize: 80 }} /></Button>
                            <input {...getInputProps()} />
                            <h4 style={{ textAlign: 'center' }}>Arraste os arquivos para cá ou clique para seleciona-los</h4>
                        </CardItem>
                    </div>
                )}
            </Dropzone>
        </div>
    </>)
}

export default MakeUpload;
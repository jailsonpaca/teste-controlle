import React, { useState, useEffect } from 'react';
import { Row, Col, Typography } from 'antd';
import { getData } from '../lib/storage';
import CardItem from '../components/cardItem';
import { useDispatch } from 'react-redux';

interface IFiles { //Interface(Tipagem) dos arquivos para o Typescript
    key: string;
    base64: string;
    name: string;
    type: string;
}

const Uploads: React.FC = () => {
    // Página para listar os uploads já feitos
    const [files, setFiles] = useState<IFiles[]>([]);
    const setLength = useDispatch();

    useEffect(() => {
        const res = async () => {
            await getData().then((data: IFiles[]) => {
                setFiles(data);
                setLength({ type: 'ADD_FILE', data: data.length });
            }).catch((err) => {
                console.log(err);
            });
        }
        res();
    }, []);

    return (<Row wrap style={{ width: '96%', height: '100%', margin: 'auto 2%', paddingBottom: '5%' }} justify="center" align="middle"
        gutter={12} >
        {/* plugin jsx-control-statements do babel para "traduzir" 
        os operadores ternários padrão do js, para mim o código fica mais limpo e simples de entender.
        Link: https://www.npmjs.com/package/jsx-control-statements
        Choose, When, Otherwise e If */}
        <Choose>
            <When condition={files.length > 0}>
                {files?.map((file, i) => (
                    <Col key={i} lg={6} md={8} sm={12} style={{ marginTop: '3%' }} xs={24} >
                        <Choose>
                            <When condition={file?.type?.includes('image') && file?.base64?.includes('base64')}>
                                <CardItem styles={{ cursor: 'auto', height: 240 }}
                                    source={file?.base64} name={file.name} description={file.type} isPreview />
                            </When>
                            <Otherwise>
                                <CardItem styles={{ cursor: 'auto', height: 240 }}
                                    name={file.name} description={file.type} isImage={false} >
                                    <h3>Arquivo sem visualização</h3>
                                </CardItem>
                            </Otherwise>
                        </Choose>
                    </Col>
                ))
                }
            </When>
            <Otherwise>
                <Col span={24} style={{ margin: 'auto' }} >
                    <Typography.Title type="secondary" style={{ textAlign: 'center' }} >Não há arquivos enviados :(</Typography.Title>
                </Col>
            </Otherwise>
        </Choose>
    </Row >
    );
}

export default Uploads;
import { Row, Col, Typography } from 'antd';

export default function Home() {
  /* Página Inicial */
  return (
    <Row gutter={4} wrap style={{ width: '95%', height: '100%', }}
      justify="start" align="middle" >
      <Col span={24} >
        <Typography.Title style={{ textAlign: 'center' }} >Bem vindo ao teste prático - Controlle!</Typography.Title>
        <Typography.Title type="secondary" level={4} style={{ textAlign: 'center', margin: 'auto 20%' }} >
          Clique nas abas do menu ao lado para testar a funcionalidade desejada <span style={{ color: 'black' }}>{'; )'}</span>
        </Typography.Title>
      </Col>
    </Row>
  );
}

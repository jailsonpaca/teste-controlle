import React from 'react';
import { Row, Col, Typography } from 'antd';

const AboutPage: React.FC = () => {
  //Uma página de sobre bem básica
  return (
    <Row gutter={4} wrap style={{ width: '95%', height: '100%' }}
      justify="start" align="middle" >
      <Col span={24} >
        <Typography.Title style={{ textAlign: 'center' }} >Está é uma página "sobre"</Typography.Title>
        <Typography.Title type="secondary" level={4} style={{ textAlign: 'center', margin: 'auto 20%' }} >
          Apenas para manter o padrão <span style={{ color: 'black' }}>{': D'}</span>
        </Typography.Title>
      </Col>
    </Row>
  );
}

export default AboutPage;
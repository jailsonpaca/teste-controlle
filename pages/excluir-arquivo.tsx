import React, { useState, useEffect } from 'react';
import { Row, Col, Button, Typography } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import { getData } from '../lib/storage';
import CardItem from '../components/cardItem';
import { useDispatch } from 'react-redux';

interface IFiles {
    key: string;
    base64: string;
    name: string;
    type: string;
}

const DeleteUpload: React.FC = () => {
    //Página para excluir os uploads já feitos
    const [files, setFiles] = useState<IFiles[]>([]); //Define o tipo de dado para o useState do React(não é obrigatório em todos os casos)
    const deleteFile = useDispatch();
    const setLength = useDispatch();

    useEffect(() => {
        const res = async () => {
            await getData().then((data: IFiles[]) => {
                setFiles(data);
                setLength({ type: 'ADD_FILE', data: data.length });
            }).catch((err) => {
                console.log(err);
            });
        }
        res();
    }, []);

    function handleDelete(key) {
        deleteFile({
            type: 'ASYNC_DELETE_FILE',
            key
        });
        setFiles(files.filter(item => item.key !== key));
    }

    return (<Row wrap style={{ width: '96%', height: '100%', margin: 'auto 2%', paddingBottom: '5%' }}
        justify="center" align="middle" gutter={12}  >
        <Choose>
            <When condition={files.length > 0}>
                {files?.map((file, i) => ( // Operador "?" certifica que a função map consegue ser chamada a partir do array(útil em alguns casos para evitar erros no js)
                    <Col key={i} lg={6} md={8} sm={12} style={{ marginTop: '3%' }} xs={24} >
                        <Choose>
                            <When condition={file?.type?.includes('image')}>
                                <CardItem styles={{ cursor: 'auto', }}
                                    source={file.base64} name={file.name} description={file.type} isPreview
                                    actions={[
                                        <Button onClick={() => handleDelete(file.key)} type="text" block
                                            style={{ color: "#f5222d", height: '100%' }} >
                                            Excluir<DeleteOutlined key="delete" /></Button>,]} />
                            </When>
                            <Otherwise>
                                <CardItem styles={{ cursor: 'auto', height: 240, }}
                                    name={file.name} description={file.type} isImage={false}
                                    actions={[<Button onClick={() => handleDelete(file.key)} type="text" block
                                        style={{ color: "#f5222d", height: '100%' }} >
                                        Excluir<DeleteOutlined key="delete" /></Button>,]}>
                                    <h3>Arquivo sem visualização</h3>
                                </CardItem>
                            </Otherwise>
                        </Choose>
                    </Col>
                ))}</When>
            <Otherwise>
                <Col span={24} style={{ margin: 'auto' }} >
                    <Typography.Title type="secondary" style={{ textAlign: 'center' }} >Não há arquivos para excluir :(</Typography.Title>
                </Col>
            </Otherwise>
        </Choose>
    </Row>
    );
}

export default DeleteUpload;
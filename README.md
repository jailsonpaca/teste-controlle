# Teste Controlle

Teste prático para vaga na Controlle - Tecnologia

## Como iniciar?

1. Rode yarn ou npm install
2. Inicie o servidor
3. Rode yarn dev ou npm run dev

## Alterações feitas no servidor

- Criada rota de 'http://localhost:3001/api/v1/attachments/delete' para excluir arquivos(recebe key como parâmetro)
- Adicionado retorno da quantidade de arquivos nas rotas de upload e delete:
  - { message: 'Adicionado com sucesso', length }
  - { message: 'removido com sucesso', length }
- Removida linha src/index.js 11: app.use(bodyParser.json()) por conta de problemas na hora de fazer requisições POST

> **[Link do repositório modificado](https://gitlab.com/jailsonpaca/teste-controlle-server)**

## Libs utilizadas que foram solicitadas

- [x]Redux
- [x]Sagas
- [x]AntDesign
- [x]Babel
- [x]Axios

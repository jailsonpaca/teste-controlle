import React from "react";
/*
  Hook para obter o tamanho da página em uma aplicação server-side-rendering(SSR)
  em páginas sem SSR podemos simplesmente obter o width e o height através do objeto window,
  porém em SSR temos que nos certificar de que o objeto window já existe
  ou se a aplicação ainda está sendo renderizada no servidor
*/
export default function useWindowSize() {
  const isSSR = typeof window === "undefined";

  const [windowSize, setWindowSize] = React.useState({
    width: isSSR ? 1200 : window.innerWidth,
    height: isSSR ? 800 : window.innerHeight, isSSR
  });

  function changeWindowSize() {
    setWindowSize({ width: window.innerWidth, height: window.innerHeight, isSSR });
  }

  React.useEffect(() => {
    window.addEventListener("resize", changeWindowSize);

    return () => {
      window.removeEventListener("resize", changeWindowSize);
    };
  }, []);

  return windowSize;
}
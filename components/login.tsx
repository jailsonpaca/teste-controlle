import React, { useState } from 'react';
import { Spin, Form, Input, Button, Row, Col } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
const FormItem = Form.Item;

interface ILoginProps {
    setLogged: (v: boolean) => void;
}

const LoginPage: React.FC<ILoginProps> = ({ setLogged }) => {
    //Componente de login para quando o usuário não estiver logado(cookie=false)
    const [loading, setLoading] = useState(false);

    function handleSubmit() {
        setLoading(true);
        setTimeout(() => { setLoading(false); setLogged(true); }, 1500);
    }

    return (

        <Row style={{ width: '100%' }} align="middle" justify="center">
            <Col md={5} xs={20} >
                <Spin spinning={loading}>
                    <Form onFinish={handleSubmit}  >
                        <FormItem hasFeedback>
                            <Input addonBefore={<UserOutlined />} placeholder="Seu Email" type="text" />
                        </FormItem>
                        <FormItem hasFeedback>
                            <Input addonBefore={<LockOutlined />} placeholder="Digite sua senha" type="password" />
                        </FormItem>
                        <FormItem>
                            <Button type="primary" block htmlType="submit" className="cert-btn">Entrar</Button>
                        </FormItem>
                    </Form>
                </Spin>
            </Col>
        </Row>
    );
}

export default LoginPage;
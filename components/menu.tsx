import React, { useEffect } from 'react';
import router from 'next/router'; //Responsável pela mudança das rotas
import { Menu as AntdMenu, Switch, Button, Row, Col, Badge } from 'antd';
import {
    FileTextOutlined, InfoCircleOutlined, MenuUnfoldOutlined,
    MenuFoldOutlined, HomeOutlined
} from '@ant-design/icons';
import { useSelector, useDispatch } from 'react-redux';

interface IMenuProps {
    open: boolean;
    setOpenMenu: (v: boolean) => void;
}

const Menu: React.FC<IMenuProps> = ({ open, setOpenMenu }) => {
    //componente menu usado em todas as páginas através do Layout
    const { theme } = useSelector(state => state.themes);
    const { page } = useSelector(state => state.pages);
    const { files = 0 } = useSelector(state => state.files);
    const setTheme = useDispatch();
    const setPage = useDispatch();
    function changeTheme(v: boolean) {//Mudança de temas
        if (v) {
            setTheme({ type: 'CHANGE_THEME', value: 'dark' });
        } else {
            setTheme({ type: 'CHANGE_THEME', value: 'light' });
        }
    }

    function navigate(e) {
        setPage({ type: 'CHANGE_PAGE', value: e.key });//navegação
    }

    useEffect(() => {
        const paths = ["/", "/arquivos-enviados", "/fazer-upload", "/excluir-arquivo", "/sobre"];
        setPage({ type: 'CHANGE_PAGE', value: String(paths.indexOf(router.pathname) + 1) });
    }, [router.pathname]); //Verificação da rota, com isso forçamos o menu a sempre se adequar a rota/página atual

    return (
        <><Row style={{ padding: '10%' }} justify="space-between" gutter={8}  >
            <If condition={open} >
                <Col span={8} xs={12}>
                    <Switch
                        checked={theme === 'dark'}
                        onChange={(v) => changeTheme(v)}
                        checkedChildren="Dark"
                        unCheckedChildren="Light" />
                </Col></If>
            <Col sm={open ? 8 : 16} style={{ margin: !open && 'auto' }} xs={0} >
                <Choose>
                    <When condition={open} >
                        <Button onClick={() => setOpenMenu(false)} ><MenuFoldOutlined /></Button>
                    </When>
                    <Otherwise>
                        <Button onClick={() => setOpenMenu(true)}><MenuUnfoldOutlined /></Button>
                    </Otherwise>
                </Choose>
            </Col>
        </Row>
            <AntdMenu
                theme={theme}
                onClick={(v) => navigate(v)}
                defaultOpenKeys={['sub1']}
                selectedKeys={[page]}
                mode="inline" >
                {/* Com o shallow:true podemos navegar sem a necessidade de recarregar a página no browser
                Link: https://nextjs.org/docs/api-reference/next/router */}
                <AntdMenu.Item key="1" icon={<HomeOutlined />} onClick={() => router.push("/", null, { shallow: true })}>
                    Início</AntdMenu.Item>
                <AntdMenu.SubMenu key="sub1" icon={<FileTextOutlined />} title="Arquivos">
                    <AntdMenu.Item key="2" onClick={() => router.push("/arquivos-enviados", null, { shallow: true })}>
                        Arquivos Enviados<Badge count={files} offset={[10, -3]} /></AntdMenu.Item>
                    <AntdMenu.Item key="3" onClick={() => router.push("/fazer-upload", null, { shallow: true })} >
                        Fazer upload</AntdMenu.Item>
                    <AntdMenu.Item key="4" onClick={() => router.push("/excluir-arquivo", null, { shallow: true })}>
                        Excluir arquivos</AntdMenu.Item>
                </AntdMenu.SubMenu>
                <AntdMenu.Item key="5" icon={<InfoCircleOutlined />} onClick={() => router.push("/sobre", null, { shallow: true })}>
                    Sobre</AntdMenu.Item>
            </AntdMenu>
        </>
    );
}

export default Menu;
import React, { useEffect, useState } from 'react';
import { Card, Image } from 'antd';
import { EyeOutlined } from '@ant-design/icons';

interface ICardItem {   //Interface(tipagem) para as props do componente
    styles?: any;
    source?: Promise<string | ArrayBuffer> | string;
    name?: string;
    description?: string;
    actions?: any;
    isPreview?: boolean;
    isImage?: boolean;
}
/*
Esse tipo de tipagem é util na hora de chamar o componente, por exemplo:

    interface IComponente {
        prop1:tipo1;
        prop2:tipo2;
        prop3:tipo3;
    }
    <Componente prop1={prop1} prop2={prop2} prop3={prop3}>

*/

const CardItem: React.FC<ICardItem> = ({ styles = {}, source = null, name = '', description = '',
    actions = [], isImage = true, isPreview = false, children }) => {
    //Componente card que vai ser utilizado nas páginas de "arquivos enviados", "fazer upload" e " excluir arquivos"
    
    const [image, setImage] = useState('');

    useEffect(() => {
        const waitImage = async () => {
            if (typeof source !== 'string') {
                source.then((image: string) => {
                    setImage(image);
                })
            } else {
                setImage(source);
            }
        }
        if (source) {
            waitImage();
        }
    }, [source]);

    return (<>
        <Choose>
            <When condition={isImage}>
                <Card hoverable style={{ ...styles }}
                    cover={<Image alt={name} style={{ width: '100%', minHeight: 120, maxHeight: 160, height: 140, objectFit: 'cover' }} preview={isPreview && { mask: <EyeOutlined /> }}
                        src={image} />} className="cardItem"
                    actions={actions} bodyStyle={{
                        padding: '2% 5%', textAlign: 'center', width: '100%',  //bottom: '5%', position: isEmpty(actions) ? 'absolute' : 'relative',
                    }} >
                    <If condition={name !== ''} ><Card.Meta title={name} description={description} /></If>
                </Card>
            </When>
            <Otherwise>
                <Card hoverable style={{ ...styles }}
                    className="cardItem"
                    actions={actions} >{children}
                </Card>
            </Otherwise>
        </Choose></>
    );
}

export default CardItem;
import React from 'react';
import { Layout, Button, Typography, Row, Col } from 'antd';
import { LogoutOutlined } from '@ant-design/icons';
import useWindowSize from './utils/useWindowSize';
import { MenuUnfoldOutlined, MenuFoldOutlined } from '@ant-design/icons';

interface IHeader {
    theme: string;
    setLogged: (v: boolean) => void;
    openMenu: boolean;
    setOpenMenu: (v: boolean) => void;
}

const Header: React.FC<IHeader> = ({ theme, setLogged, openMenu, setOpenMenu }) => {
    //Componente header que vai ser utilizado no layout das páginas

    const { width } = useWindowSize();

    return (
        <Layout.Header style={{
            backgroundColor: theme === 'light' ? '#fff' : '#001529', padding: 0,
        }} ><Row justify="center" align="middle" >
                {/* Verifica se é dispositivo Mobile o não */}
                <If condition={width < 600} >
                    <Col offset={1} xs={4}  >
                        <Choose>
                            <When condition={openMenu} >
                                <Button onClick={() => setOpenMenu(false)} ><MenuFoldOutlined /></Button>
                            </When>
                            <Otherwise>
                                <Button onClick={() => setOpenMenu(true)}><MenuUnfoldOutlined /></Button>
                            </Otherwise>
                        </Choose>
                    </Col>
                </If>
                <Col offset={width > 600 && 4} md={16} xs={12}>
                    <Typography.Title level={width > 600 ? 3 : 5}
                        style={{ textAlign: width > 600 ? 'center' : 'end', color: theme !== 'light' ? '#fff' : '#001529' }} >
                        Teste Prático - Controlle</Typography.Title>
                </Col>
                <Col md={4} offset={width < 600 && 1} xs={6}  >
                    <Button onClick={() => setLogged(false)} style={{ margin: 'auto' }} >Sair<LogoutOutlined /></Button>
                </Col>
            </Row>
        </Layout.Header >
    );
}

export default Header;
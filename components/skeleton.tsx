import React from 'react';
import { Skeleton as AntdSkeleton } from 'antd';

const Skeleton: React.FC = () => {

    return (<div style={{ margin: '5%' }} >
        <AntdSkeleton active />
        <AntdSkeleton active />
        <AntdSkeleton active />
        <AntdSkeleton active />
    </div>
    );
}

export default Skeleton;
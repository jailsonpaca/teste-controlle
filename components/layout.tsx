import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Menu from './menu';
import LoginPage from './login';
import { Layout, Drawer, notification, message } from 'antd';
import { useSelector } from 'react-redux';
import cookieCutter from 'cookie-cutter';
import Header from './header';
import useWindowSize from './utils/useWindowSize';
import Skeleton from './skeleton';

const { Sider, Content } = Layout;

const LayoutComponent: React.FC = ({ children }) => {

    //Componente Layout usado em todas as páginas
    const { theme } = useSelector(state => state.themes);
    const { messages } = useSelector(state => state.messages);
    const [openMenu, setOpenMenu] = useState(true);
    const [logged, setLogged] = useState<boolean>(false);
    const { width } = useWindowSize();

    useEffect(() => {
        if (typeof window !== 'undefined') {
            setLogged(false || cookieCutter.get('logged') === 'true');//verifica o cookie se está logado ou não-logado
            console.log('cookie(logged): ', cookieCutter.get('logged') === 'true');
        }
    }, [])

    useEffect(() => {
        if (typeof window !== 'undefined') {
            cookieCutter.set('logged', logged); //modifica o cookie para logado ou não-logado
        }
    }, [logged]);

    useEffect(() => {
        const openNotification = () => {
            const args = {
                message: messages.value,
                //duration: 0,
            };
            if (messages.error) {
                notification.error(args);
            } else {
                notification.success(args);
            }
        };
        if (messages.value !== '') {
            openNotification();
        }
    }, [messages]);

    const router = useRouter();
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const handleStart = (url) => (url !== router.asPath) && setLoading(true);//Carrega a página com um Skeleton 
        const handleComplete = (url) => (url === router.asPath) && setLoading(false);//Termina de carregar a página com um Skeleton 
        /* Verifica o status do carregamento das páginas */
        router.events.on('routeChangeStart', handleStart)
        router.events.on('routeChangeComplete', handleComplete)
        router.events.on('routeChangeError', handleComplete)

        return () => {
            router.events.off('routeChangeStart', handleStart)
            router.events.off('routeChangeComplete', handleComplete)
            router.events.off('routeChangeError', handleComplete)
        }
    });

    return (<>
        <Choose>
            <When condition={!logged} >
                <LoginPage setLogged={setLogged} />
            </When>
            <Otherwise>
                <Layout style={{ width: '100%' }} >
                    <Choose>
                        <When condition={width < 600} >
                            <Drawer
                                bodyStyle={{ padding: 0, backgroundColor: theme === 'light' ? '#fff' : '#001529', }}
                                placement="left"
                                closable={false}
                                onClose={() => setOpenMenu(false)}
                                visible={openMenu}>
                                <Menu open={openMenu} setOpenMenu={setOpenMenu} />
                            </Drawer>
                        </When>
                        <Otherwise>
                            <Sider trigger={null} collapsible collapsed={!openMenu} style={{ zIndex: 10 }}
                                theme={theme} ><Menu open={openMenu} setOpenMenu={setOpenMenu} /></Sider>
                        </Otherwise>
                    </Choose>
                    <Layout style={{ zIndex: 9 }} >
                        <Header theme={theme} setLogged={setLogged} openMenu={openMenu} setOpenMenu={setOpenMenu} />
                        <Content>{/* Carrega o skeleton ou a página */}
                            <Choose>
                                <When condition={loading}>
                                    <Skeleton />
                                </When>
                                <Otherwise>
                                    {children}
                                </Otherwise>
                            </Choose>
                        </Content> {/* TODAS as páginas passam por esse componente */}
                    </Layout>
                </Layout>
            </Otherwise>
        </Choose>
    </>
    );
}

export default LayoutComponent;